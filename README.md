# News Genius

News Genius is a web application that offers users an interactive and personalized experience when it comes to staying up-to-date with the latest news. Our chatbot, "Genie", can announce the latest news, summarize articles, display them, or redirect users to the full article.

# Features
Chat with "Genie" to get the latest news and updates
Choose the type of news you want based on different categories
Get quick summaries of articles or redirect to the full article
Stay up-to-date with the latest news in a convenient and interactive way
Getting Started
To get started with News Genius, simply go to our website and start chatting with "Genie". You can select the type of news you want to receive and "Genie" will provide you with the latest updates.

# Contributing
We welcome any contributions to News Genius. If you would like to contribute, please submit a pull request and we will review it.

# Support
If you need any help or have any questions, please don't hesitate to reach out to us at :
- paul.bertrand@efrei.net
- elliott.joliman@efrei.net

# License
News Genius is licensed under the MIT License.

https://newsapi.org/docs