from django.shortcuts import render

from django.http import JsonResponse
from .genie import genie

def chat_page(request):
    return render(request, 'page.html')

def chatbot(request):
  if request.method == 'POST':
    input_data = request.POST.get('input', '')
    response = genie(input_data)
    return JsonResponse({'response': response})
  else:
    return JsonResponse({'error': 'Invalid request method'})