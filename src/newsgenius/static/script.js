// Get the chat input field and chat messages container
const chatInput = document.querySelector('#chat-input');
const chatMessages = document.querySelector('#chat-messages');

// Function to display a message in the chat
function displayMessage(message, type) {
  // Create a div for the message
  const messageDiv = document.createElement('div');
  messageDiv.classList.add('chat-message');
  messageDiv.classList.add(type);
  
  // Create a p tag for the message text
  const messageText = document.createElement('p');
  messageText.innerText = message;
  
  // Add the message text to the message div
  messageDiv.appendChild(messageText);
  
  // Add the message div to the chat messages container
  chatMessages.appendChild(messageDiv);

  // Scroll to the bottom of the chat messages container
  chatMessages.scrollTop = chatMessages.scrollHeight;
}

// Function to handle the user's input
function handleInput(e) {
  // Prevent the form from submitting
  e.preventDefault();
  
  // Get the user's input
  const userInput = chatInput.value;
  if(userInput!==""){
  // Display the user's message in the chat
  displayMessage(userInput, 'user-message');
  
  // Clear the input field
  chatInput.value = '';

  // Get the CSRF token cookie
  const csrftoken = document.cookie.match(/csrftoken=([\w-]+)/)[1];

  const body = new URLSearchParams();
  body.append('input', userInput);
  
  fetch('/chatbot/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-CSRFToken': csrftoken
    },
    body: body.toString()
  })
  .then(response => response.json())
  .then(data => {
    const chatbotResponse = data.response;
    displayMessage(chatbotResponse, 'chatbot-message');
  })
  .catch(error => console.error(error));
  }
  else{
    None
  }

}

// Listen for the enter key to be pressed in the chat input field
chatInput.addEventListener('keypress', function (e) {
  if (e.key === 'Enter') {
    handleInput(e);
  }
});