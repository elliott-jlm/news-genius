import openai
import requests
import re
import os
import json

import nltk
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer

from google.oauth2 import service_account
from google.cloud import dialogflow_v2 as dialogflow

import json
import proto



def country_code(country):

    code = ''
    # Load the JSON data from file
    with open('countries.json') as f:
        data = json.load(f)

    # Check if the country is in the dictionary and return its value
    if country.lower() in data:
        code = data[country.lower()]
    else:
        code = ''
    return code

def find_words_capital(input):

    # pattern for extracting words
    pattern = r"(?<!^)(?<!\.)\b[A-Z][a-z]+\b"
    words_capital_list = re.findall(pattern, input)

    return words_capital_list


def make_url(category, final_q, from_date, q_type):
    """
    This function generates a URL to fetch news articles from the newsdata.io API.
    
    Parameters:
    - country (str): the 2-letter country code where the news should come from
    - category (str): the news category, e.g. 'business', 'technology', etc.
    - q (str): the query string used to search for specific news articles
    - from_date (str): the start date for the news articles, in the format 'YYYY-MM-DD'

    Returns:
    - str: the generated URL

    """
    # The base URL for the newsdata.io API
    url = "https://newsdata.io/api/1/news?"

    if q_type == 'q':
        # The parameters for the API
        params = {
        'apikey' : 'pub_1708436379883ede365a3878c13fd29253a38',
        'category' : category,
        'q' : final_q,
        'from_date' : from_date
        }
    
    else:
        # The parameters for the API
        params = {
        'apikey' : 'pub_1708436379883ede365a3878c13fd29253a38',
        'category' : category,
        'qInTitle' : final_q,
        'from_date' : from_date
        }  

    # Loop through the parameters and add them to the URL if they are not empty
    for key, value in params.items():
        if value != '':
            url += f'{key}={value}&'

    # Remove the trailing '&' from the URL
    url = re.sub(r'&$', '', url)

    return url


def news(q, category, country, from_date, q_type):
    """
    This function retrieves the first news article matching the given criteria from the newsdata.io API.
    
    Parameters:
    - q (str): the query string used to search for specific news articles
    - category (str): the news category, e.g. 'business', 'technology', etc.
    - country (str): the 2-letter country code where the news should come from
    - from_date (str): the start date for the news articles, in the format 'YYYY-MM-DD'

    Returns:
    - dict: a dictionary containing the retrieved article information, including the link, title, description, and content

    """
    # Set a default value for the article
    article = {}
    
    # Generate the query URL
    query_url = make_url(category, q, from_date, q_type)
    
    try:
        # Make a GET request to the API
        response = requests.get(query_url)
        # Check the status code of the response
        if response.status_code == 200:
            # Make a GET request to the API and retrieve the first result
            
            results = response.json()['results']
            i = 0
            while i < len(results) and (results[i]['title'] is None or results[i]['link'] is None or results[i]['content'] is None or results[i]['language'] is None):
                i += 1
            if i == len(results):
                # No results found with non-null title, link, and content
                None
            
            else:
                # Extract the relevant information from the first result with non-null fields
                link = results[i]['link']
                title = results[i]['title']
                content = results[i]['content']
                language = results[i]['language']
            
            # Create a dictionary to store the article information
            article = { 'link' : link, 'title' : title, 'content' : content, 'language' : language}
    except:
        # Handle any exceptions that may occur
        print("Failed to fetch news data")
        
    return article


key = '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'
openai.api_key = key


def complete(input):
  input = "Your Name is Genie, your are a News Chatbot. Here is my question : "+ input
  response = openai.Completion.create(
      engine="text-davinci-002",
      prompt=input,
      max_tokens=256,
      n=1,
      stop=None,
      temperature=0.5,
  )

  answer = response.choices[0].text.strip()

  return answer

def translate(input):
  input = "You are a translator program can you translate this text to english : "+ input
  response = openai.Completion.create(
      engine="text-davinci-002",
      prompt=input,
      max_tokens=2000,
      n=1,
      stop=None,
      temperature=0.5,
  )

  answer = response.choices[0].text.strip()

  return answer


def remove_stop_word(text):
    text_clean = ''
    words = nltk.word_tokenize(text)
    # Get the set of English stop words
    stop_words = set(stopwords.words('english'))

    # Remove the stop words from the sentence
    filtered_words = [word for word in words if word.lower() not in stop_words]

    # Join the filtered words back into a sentence
    text_clean = ' '.join(filtered_words)
    return text_clean



def tree_main_words(text_clean):
    # Initialize the TF-IDF vectorizer
    vectorizer = TfidfVectorizer()

    # Calculate the TF-IDF scores for the words in the sentence
    tfidf = vectorizer.fit_transform([text_clean])

    # Get the feature names (i.e., the words in the sentence)
    try:
        feature_names = vectorizer.get_feature_names()
    except AttributeError:
        feature_names = list(vectorizer.vocabulary_.keys())

    # Get the TF-IDF scores for each word in the sentence
    tfidf_scores = tfidf.toarray()[0]

    # Get the indices of the three highest-scoring words
    top_indices = tfidf_scores.argsort()[-3:][::-1]

    # Get the top three words with the highest TF-IDF scores
    top_words = [feature_names[i] for i in top_indices]
    return top_words



def create_query(text, parameters):

    text_clean = remove_stop_word(text)
    top_words = tree_main_words(text_clean)

    p, p1 = parameters['person'], parameters['person1']
    if p != '' and p1 != '':
        p, p1 = parameters['person']['name'].lower(), parameters['person1']['name'].lower()
    elif p == '' :
        p, p1 = '', ''
    elif p != '' and p1 == '':
        p, p1 = parameters['person']['name'].lower(), ''
    
    city = parameters['geo-city'].lower()
    key_words = [p, p1, city]

    for k in key_words:
        if k not in top_words and k != '': 
            top_words.append(k)

    query = " ".join(top_words)
    return query

def create_final_query(input, parameters):
    q_type = ''
    q = ''
    words_capital = find_words_capital(input)

    #Si personne / ville / pays dans input => qInTitle
    if parameters['geo-country'] != []:
        for country in parameters['geo-country']:
            q += country + ' '
    if parameters['geo-city'] != '':
        q += parameters['geo-city'] + ' '

    if parameters['person'] != '':
        q += parameters['person']['name'] + ' '

    if parameters['person1']!= '':
        q += parameters['person1']['name'] + ' '

    q_list = q.strip().split(' ')
    for word in words_capital:
        if word not in q_list:
            q += word + ' '

    q_list = q.split(' ')
    q_list = list(set(q_list))
    
    q_list = [item for item in q_list if item != ""]
    q = ' '.join(q_list)

    q_type = 'qInTitle'
    #Sinon q avec les trois mots les plus importants
    if q == "":
        q = create_query(input, parameters)
        q_type = 'q'
    return q.strip(), q_type

def dialogflow_output(input):
    # Set environment variable with path to your service account key file
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = './newagent-gtpb-eb3d4f8dfe4f.json'

    # Authenticate with the Dialogflow API
    credentials = service_account.Credentials.from_service_account_file(os.environ['GOOGLE_APPLICATION_CREDENTIALS'])
    client = dialogflow.SessionsClient(credentials=credentials)

    # Set up the request
    project_id = "newagent-gtpb"
    session_id = "146578282734"
    session = client.session_path(project_id, session_id)
    text_input = dialogflow.types.TextInput(text=input, language_code='en-US')
    query_input = dialogflow.types.QueryInput(text=text_input)

    # Send the request and get the response
    response = client.detect_intent(session=session, query_input=query_input)

    # Print the response
    dialogflow_response = response.query_result

    js = proto.Message.to_json(dialogflow_response)
    my_dict = json.loads(js)
    
    question = my_dict['queryText']
    intent = my_dict['intent']['displayName']
    parameters = my_dict['parameters']
    print(f"params : {parameters}\n\n")
    return question, intent, parameters


def actuality(question, parameters):
    query = create_final_query(question, parameters)[0]
    print(f"query : {query}\n")
    q_type = create_final_query(question, parameters)[1]
    print(f"q_type : {q_type}\n")
    if parameters['Categorie'] != [] and parameters['geo-country'] != []:
        article = news(q=query, category=', '.join(parameters['Categorie']), country=parameters['geo-country'][0], from_date=parameters['date-time'], q_type=q_type)
    
    elif parameters['Categorie'] != [] and parameters['geo-country'] == []:
        article = news(q=query, category=', '.join(parameters['Categorie']), country='', from_date=parameters['date-time'], q_type=q_type)
    
    elif parameters['Categorie'] == [] and parameters['geo-country'] != []:
        article = news(q=query, category='', country=parameters['geo-country'][0], from_date=parameters['date-time'], q_type=q_type)        
    elif parameters['Categorie'] == [] and parameters['geo-country'] == []:
        article = news(q=query, category='', country='', from_date=parameters['date-time'], q_type=q_type)
        
    return article


def genie(user_input):

    question, intent, parameters = dialogflow_output(user_input)
    output = ''

    if intent == 'Actuality': 
        # get the actuality result through the datanews.io API
        output = '[NEWS]\n\n'

        article = actuality(question, parameters)

        if article == {}:
            output += "Oops, I unfortunately did not find an article :/"
        
        else :
            title = article['title']
            content = article['content']
            link = article['link']

            if article['language'] != 'en':
                output += translate(title) + '\n\n' + translate(content) + '\n\n' + link

            else:
                output += title + '\n\n' + content + '\n\n' + link
    else:
        # get a response with the Openai completion API
        output += complete(user_input)
        
    return output